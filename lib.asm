section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define NEW_LINE '\n'
%define TAB_SYMBOL 9
%define SPACE_SYMBOL 20

%define READ 0
%define WRITE 1
%define EXIT 60

%define STDIN 0
%define STDOUT 1
%define CHAR_NEWLINE 0xA

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, -1 ; string length
.loop:
    inc rax
    cmp byte[rdi+rax], 0
    jnz .loop
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    mov rax, WRITE
    mov rsi, rdi
    mov rdi, STDOUT
    syscall
    pop rdi
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, WRITE
    mov rsi, rsp
    mov rdx, STDOUT
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov dil, CHAR_NEWLINE
	call print_char
	ret 

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push rbp
	push r12
	push rsp
    mov rbp, rsp
	mov rax, rdi
	mov r12, 10
	sub rsp, 1
	mov byte[rsp], 0
.loop:
    xor rdx, rdx
    div r12
    add rdx, '0'
    sub rsp, 1
    mov byte [rsp], dl
    test rax, rax
    jz .print
    jmp .loop
.print:
    mov rdi, rsp
    call print_string
    mov rsp, rbp
	pop rsp
    pop r12
    pop rbp
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .out
	push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
	call print_uint
	ret
.out:
    call print_uint
	ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.com:
    mov cl, byte[rdi]
    cmp cl, byte[rsi]
    je .next
    ret

.next:
    cmp byte[rsi], 0x0
    je .ret_true
    inc rdi
    inc rsi
    jmp .com

.ret_true:
    inc rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rsi
    push rdi
    push rdx
    push 0x0
.read:
    mov rsi, rsp
    mov rdi, STDIN
    mov rdx, 1
    mov rax, READ
    syscall
.end:
    pop rax
    pop rdx
    pop rdi
    pop rsi
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	mov r8, rdi  
	mov r9, rsi  
	xor r10, r10
.space:
	call read_char
	cmp rax, SPACE_SYMBOL
	je .space
	cmp rax, TAB_SYMBOL
	je .space
	cmp rax, CHAR_NEWLINE
	je .space
	je .end
	test rax, rax

.loop:
	cmp r9, r10
	je .fail
	test rax, rax
	jz .end

	mov [r8+r10], rax
	inc r10
	call read_char
	cmp rax, SPACE_SYMBOL
	je .end
	cmp rax, TAB_SYMBOL
	je .end
	cmp rax, CHAR_NEWLINE
	je .end
	
	jmp .loop 

.fail:
	xor rax, rax
	ret
.end:
	mov rax, r8
	mov rdx, r10
	mov byte[r8+r10],0
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax,rax
    xor r8, r8
    xor rdx, rdx
    mov r10, 10

.loop:
    mov r8b,byte[rdi+rdx]
    cmp r8b, '0'
    jb .end
    cmp r8b, '9'
    ja .end
    sub r8b, '0'
    push rdx
    mul r10
    pop rdx
    add rax, r8
    inc rdx
    jmp .loop
    .end:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	cmp byte [rdi], '-'
	je .is_neg
	jmp parse_uint
.is_neg:
	inc rdi
	call parse_uint
	neg rax
	test rdx, rdx
	jz .end
	inc rdx
.end:
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
.loop:
      xor rax, rax
      mov al, byte[rdi]
      mov byte[rsi], al
      inc rdi
      inc rsi
      test rax, rax
      jnz .loop
      ret
is_whitespace:
    cmp rdi, ' ' 		; Пробел
    je .whitespace
    cmp rdi, '	' 	; Табуляция
    je .whitespace
    cmp rdi, '\n' 		; Перевод строки
    je .whitespace
    xor rax, rax
    ret
.whitespace:
    mov rax, 1
    ret
