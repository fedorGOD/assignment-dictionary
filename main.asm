%include "words.inc"
%include "lib.inc"

%define SIZE_BUF 255
%define SIZE_WORD 8
extern find_word

section .data
size_error: db "Максимальная длина 255", 10, 0
key_error: db "Ключ не найден", 10, 0
buf: times 255 db 0

section .text
global _start
print_error:
  call string_length
  mov rdx, rax
  mov rax, 1
  mov rsi, rdi
  mov rdi, 2
  syscall
  ret

_start:
  mov rdi, buf
  mov rsi, SIZE_BUF
  call read_word
  test rax, rax
  jz .size_err
  mov rdi, rax
  mov rsi, last
  call find_word
  test rax, rax
  jz .key_err
  mov rdi, rax
  add rdi, SIZE_WORD
  push rdi
  call string_length
  pop rdi
  inc rax
  add rdi,rax
  call print_string
  call print_newline
  call exit
.size_err:
  mov rdi, size_error
  call print_error
  call exit
.key_err:
  mov rdi, key_error
  call print_error
  call exit